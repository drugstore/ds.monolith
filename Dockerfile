FROM golang:1.18-alpine

EXPOSE 8080

WORKDIR /app

RUN go mod init ds.monolith && go mod tidy

COPY *.go ./
# RUN go mod download
RUN go build -o main

CMD [ "./main" ]
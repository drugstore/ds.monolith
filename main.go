package main

import (
	"encoding/json"
	"log"
	"net/http"
)

func main() {
	// fmt.Println("Hello ds.monolith!")
	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		type response struct {
			Success bool `json:"success"`
		}

		res, err := json.Marshal(response{
			Success: false,
		})

		if err != nil {
			log.Fatal(err)
			w.Write(res)
			return
		}

		w.Write(res)
	})

	http.ListenAndServe(":8080", nil)
}
